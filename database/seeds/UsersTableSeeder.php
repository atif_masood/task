<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('role_user')->truncate();

        $adminRole = Role::where('name','admin')->first();
        $userRole = Role::where('name','user')->first();

        $admin = User::create([
        	'f_name' => 'Admin',
        	'l_name' => 'User',
            'email' => 'admin@gmail.com',
            'phone' => '03075207206',
            'gender' => 'Male',
            'dob' => '1996-11-01',
        	'password' => Hash::make('atif1996')
        ]);

        $user = User::create([
        	'f_name' => 'todo',
        	'l_name' => 'todo',
        	'email' => 'todo@gmail.com',
        	'phone' => '03075207206',
            'gender' => 'Male',
            'dob' => '1996-11-01',
        	'password' => Hash::make('password123')
        ]);

        $admin->roles()->attach($adminRole);
        $user->roles()->attach($userRole);
    }
}
