<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::prefix('admin')->name('admin.')->middleware('can:admin')->group(function(){
	Route::resource('/users', 'Admin\UserController',['except' => ['show', 'create' , 'store']]);
});

Route::prefix('user')->name('user.')->middleware('can:user')->group(function(){
	Route::resource('/users', 'User\UserController',['except' => ['show', 'create' , 'store']]);

	Route::get('/item', 'ItemController@index'); 
	Route::get('/item/edit/{id}', 'ItemController@edit_item');  
	Route::post('/edit_item', 'ItemController@post_edit_item'); 
	Route::get('/add_item', 'ItemController@add_item');   
	Route::post('/add_item', 'ItemController@post_item');
	Route::get('/item/delete/{id}', 'ItemController@delete');    
});
