<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Item;
use Illuminate\Support\Facades\Auth;

class ItemController extends Controller
{
    public function index()
    {
    	$user_id = Auth::id();
    	if(Auth::user()->hasRole('admin'))
    	{
    		$items= Item::get();
    	}
    	elseif(Auth::user()->hasRole('user'))
    	{
    		$items= Item::where('user_id',$user_id)->get();
    	}
    	
        return view('admin.items.index', compact('items'));
    }

    public function edit_item($id)
    {
        try {
            $item = Item::FindorFail($id);
            // dd($expense);
        } catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage());
        }
        return view('admin.items.edit_item',compact('item'));
    }

      

    public function post_edit_item(Request $request)
    {
        $this->validate($request,[  
            'name'=>'required',
            'description'=>'required', 
            'category'=>'required',
        ]);
        $id = $request->id;
        $item= Item::where('id', $id)->first();
        $item->status = $request->status;
        $item->name = $request->name;
        $item->description = $request->description;
        $item->category = $request->category;
        $item->update();


        return redirect('user/item')->with('success','Update Successfully');

    } 

    public function add_item()
    {
        $user_id = Auth::id();
        return view('admin.items.add_item',compact('user_id'));
    }  

    public function post_item(Request $request)
    {
            $this->validate($request,[ 
                'name'=>'required',
                'description'=>'required', 
                'category'=>'required',
            ]);

            $user_id = $request->user_id;

            $item= new Item;
            $item->user_id = $user_id;
            $item->name = $request->name;
            $item->description = $request->description;
            $item->category = $request->category;

            $item->save();

            return redirect('user/item')->with('success','Item Added Successfully');
    
    }

    public function delete(Request $request,$id)
    {
        $item = Item::find($id);
        $item->delete();
        $request->session()->flash('message','Item Deleted');
        return redirect('user/item');
    }

}
