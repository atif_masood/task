@extends('layouts.user.app')
@section('title')
    <title>Items</title>
    <link href="/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
    
@endsection
@section('content')

<form action="{{ action('ItemController@post_edit_item') }}" id="submit_addassign_form" class="form-horizontal" role="form" method="post">
    <div class="form-body">
        @csrf
        <br><br>
        
        <div class="form-group">
            <label class="col-md-3 label">Item Name</label>
            <div class="col-md-6">
                <input id="id" type="hidden" name="id" value="{{$item->id}}">
                <input id="name" type="text" placeholder="Enter Item Name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $item->name }}">
                <div style="color:red;">{{$errors->first('name')}}</div> <br>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 label">Description</label>
            <div class="col-md-6">
               <input id="description" type="text" placeholder="Enter Description" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ $item->description }}">
                <div style="color:red;">{{$errors->first('description')}}</div> <br>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 label">Status</label>
            <div class="col-md-6">
               <select name="status" class="countries order-alpha form-control" id="status" value="{{$item->status}}">
                    <option status="">Select Status</option>
                    <option value="Completed" {{($item->status ==='Completed') ? 'selected' : ''}}>Completed</option>
                    <option value="Snoozed" {{($item->status ==='Snoozed') ? 'selected' : ''}}>Snoozed</option>
                    <option value="Overdue" {{($item->status ==='Overdue') ? 'selected' : ''}}>Overdue</option>
                </select>
                <div style="color:red;">{{$errors->first('description')}}</div> <br>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 label">Category</label>
            <div class="col-md-6">
               <input id="category" type="text" placeholder="Enter Category" class="form-control @error('category') is-invalid @enderror" name="category" value="{{ $item->category }}">
                <div style="color:red;">{{$errors->first('category')}}</div> <br>
            </div>
        </div>


    </div>
    <div class="form-actions right1">
        <div class="col-md-4"></div>
        <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
        <button type="submit" class="btn green">Submit</button>
    </div>
</form>
@endsection
@section('page-script')

<script src="/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/moment.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    
    
    <script src="https://www.chartjs.org/dist/2.7.3/Chart.bundle.js"></script>
    <script src="https://www.chartjs.org/samples/latest/utils.js"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        $(document).ready(function()
        {
            $('#clickmewow').click(function()
            {
                $('#radio1003').attr('checked', 'checked');
            });
            
        });
        
    </script>
@endsection
